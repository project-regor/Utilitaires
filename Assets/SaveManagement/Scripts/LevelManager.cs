﻿using System;
using System.Collections;
using Databox;
using ECM.Controllers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

#pragma warning disable 649

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    private Animator _animator;

    [SerializeField] private string defaultSceneName;
    private Vector3 _forcedPos = Vector3.negativeInfinity;
    private string _nextScene;

    [SerializeField] private UIDocument pauseMenu;
    private bool _startAnimSkipped = true;

    [SerializeField] private float transitionTime;
    [SerializeField] private GameObject player;

    public DataboxObject data;
    public GameObject playerInstance;

    private string _spawnName;

    public bool paused;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            SceneManager.sceneLoaded += LoadingDone;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        pauseMenu.rootVisualElement.Q<Button>("Save").clicked += Save;
        pauseMenu.rootVisualElement.Q<Button>("Load").clicked += Load;
        pauseMenu.rootVisualElement.Q<Button>("Quit").clicked += TitleScreen;

        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LoadingDone(Scene scene, LoadSceneMode __)
    {
        if (scene.name == "EcranTitre")
            DestroyAll();

        if (!playerInstance && scene.name != "EcranTitre")
        {
            playerInstance = FindObjectOfType<BaseCharacterController>().transform.parent.gameObject;
            playerInstance ??= Instantiate(player, player.transform.position, player.transform.rotation);
        }

        GameObject destination = GameObject.Find(_spawnName);
        if (!string.IsNullOrWhiteSpace(_spawnName) && destination != null && playerInstance != null)
            playerInstance.transform.position = destination.transform.position;

        if (playerInstance != null && !float.IsInfinity(_forcedPos.x))
        {
            playerInstance.transform.rotation = data.GetData<QuaternionType>("Player", "SAVE_01", "Rotation").Value;

            playerInstance.transform.position = _forcedPos;
            _forcedPos = Vector3.negativeInfinity;
        }
        else if (playerInstance != null && FindObjectOfType<SceneChange>() != null)
        {
            playerInstance.transform.position = FindObjectOfType<SceneChange>().transform.position;
        }

        if (!_startAnimSkipped)
            _animator.Play("Circle Transition End");

        if (playerInstance == null) return;

        playerInstance.GetComponentInChildren<Camera>().enabled = FindObjectsOfType<Camera>().Length <= 1;
        playerInstance.GetComponentInChildren<AudioListener>().enabled = FindObjectsOfType<AudioListener>().Length <= 1;
    }

    public void TitleScreen()
    {
        ChangeLevel(1, null);
    }

    public void NextLevel(bool skipTransition = false)
    {
        try
        {
            int destination = SceneManager.GetActiveScene().buildIndex + 1;
            SceneManager.GetSceneByBuildIndex(destination);
            ChangeLevel(destination, null, skipTransition);
        }
        catch (ArgumentException)
        {
            ChangeLevel(defaultSceneName, null, skipTransition);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ChangeLevel(int buildOrder, string exitDoor, bool skipTransition = false)
    {
        _nextScene = buildOrder.ToString();
        StartCoroutine(nameof(LevelLoad), new CoroutineArgs(skipTransition, exitDoor));
    }

    public void ChangeLevel(string sceneName, string exitDoor, bool skipTransition = false)
    {
        _nextScene = sceneName;
        StartCoroutine(nameof(LevelLoad), new CoroutineArgs(skipTransition, exitDoor));
    }

    private IEnumerator LevelLoad(CoroutineArgs args)
    {
        if (args.SkipTransition)
        {
            _startAnimSkipped = true;
        }
        else
        {
            _startAnimSkipped = false;
            _animator.Play("Circle Transition");
        }

        yield return new WaitForSeconds(args.SkipTransition ? 0f : transitionTime);

        if (int.TryParse(_nextScene, out int i))
            SceneManager.LoadScene(i);
        else
            SceneManager.LoadScene(_nextScene);

        _spawnName = args.ExitDoor;
    }

    public void Pause()
    {
        Time.timeScale = 0;
        paused = true;
        pauseMenu.rootVisualElement.Q<VisualElement>("Container").visible = true;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
        paused = false;
        pauseMenu.rootVisualElement.Q<VisualElement>("Container").visible = false;
    }

    public void Save()
    {
        Vector3Type pos = new Vector3Type {Value = playerInstance.transform.position};
        data.SetData<Vector3Type>("Player", "SAVE_01", "Position", pos);

        QuaternionType rot = new QuaternionType {Value = playerInstance.transform.rotation};
        data.SetData<Vector3Type>("Player", "SAVE_01", "Rotation", rot);

        IntType scene = new IntType {Value = SceneManager.GetActiveScene().buildIndex};
        data.SetData<IntType>("Player", "SAVE_01", "Scene", scene);

        UnPause();
    }

    public void Load()
    {
        UnPause();
        DestroyAll();

        _forcedPos = data.GetData<Vector3Type>("Player", "SAVE_01", "Position").Value;
        int scene = data.GetData<IntType>("Player", "SAVE_01", "Scene").Value;
        ChangeLevel(scene, null);
    }

    public void DestroyAll()
    {
        Destroy(playerInstance);
        playerInstance = null;
    }

    private class CoroutineArgs
    {
        public readonly string ExitDoor;
        public readonly bool SkipTransition;

        public CoroutineArgs(bool skipTransition, string exitDoor)
        {
            SkipTransition = skipTransition;
            ExitDoor = exitDoor;
        }
    }
}