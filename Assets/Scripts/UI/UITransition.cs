﻿using System;
using UnityEngine;

public class UITransition : MonoBehaviour
{
    public static event Action TransitionDone;

    public void LaunchEvent()
    {
        TransitionDone?.Invoke();
    }
}
