﻿using UnityEngine;
using UnityEngine.Video;

public class VideoToScene : MonoBehaviour
{
    [SerializeField] private string _destination;
    private LevelManager _levelManager;

    private VideoPlayer _video;

    // Start is called before the first frame update
    private void Start()
    {
        _levelManager = LevelManager.Instance;
        print(_levelManager);
        _video = GetComponent<VideoPlayer>();
        _video.loopPointReached += NextScene;
    }

    private void NextScene(VideoPlayer _)
    {
        _levelManager.NextLevel(true);
    }
}